Beast Mode Hunter
-----------------

Ever try to delete a beast mode and get the helpful error that other cards are using the calculation? This Chrome extension will enhance that experience, providing the following options:

1. Direct card navigation: Now you'll be presented with a list of cards with links, vs just the name.
2. Force delete: (Experimental) Remove the beast mode from the dataset. This will break any cards that were using that calculation.

TODO:
* Bulk Replace. Rather than deleting the calculation and breaking the cards, why not specify a new column or calculation to use instead, and then delete it.
