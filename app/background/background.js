'use strict';

// Receive message from content.js and post to inject.js
chrome.runtime.onMessage.addListener(function(message, sender) {
  if (message.command && message.command === 'bm-delete') {
    window.postMessage(JSON.stringify(message), window.location.origin);
  }
});

// Watch Network Traffic for Beast Mode usage request v1
chrome.webRequest.onCompleted.addListener(details => {
  if (details.tabId !== -1) { formulaUsageV1(details.url, details.tabId); }
}, { urls: ['https://*.domo.com/api/content/v1/cards/formulausage*'] }, []);

// Watch Network Traffic for Beast Mode usage request v1
chrome.webRequest.onCompleted.addListener(details => {
  if (details.tabId !== -1) { formulaUsage(details.url, details.tabId); }
}, { urls: ['https://*.domo.com/api/content/v2/cards/formulausage*'] }, []);

// Perform a new request to get a more detailed response
function formulaUsageV1(url, tabId) {
  const domo = new Domo(url);
  domo.getCardUsageV1().then(cards => {
    chrome.tabs.sendMessage(tabId, {
      beastmode: {
        dataSourceId: domo.dataSourceId,
        formulaId: domo.beastmode,
        usage: cards
      }
    });
  });
}

function formulaUsage(url, tabId) {
  const domo = new Domo(url);
  domo.getCardUsage().then(res => {
    chrome.tabs.sendMessage(tabId, {
      beastmode: {
        dataSourceId: domo.dataSourceId,
        formulaId: domo.beastmode,
        usage: res
      }
    });
  });
}

// Domo Service
function Domo(url) {
  this.server = 'https://' + getHostname(url);
  this.dataSourceId = getParameterByName('dataSourceId', url);
  this.beastmode = getParameterByName('formulaId', url);

  this.urls = {
    formulaUsage: url,
    contentDatasource: this.server + '/dataaccess/loadcontentdatasource',
    datasourceSummary: this.server + '/dataaccess/loaddatasourcesummary'
  };
}

Domo.prototype.getCardUsageV1 = function() {
  const _this = this;

  return _this.getCardUsage()
    .then(res => {
      return _this.getDatasetCards()
    })
    .then(res => {
      return _this.getCardDetails(res);
    })
    .then(res => {
      return _this.getCardsWithBeastmode(res)
    });
}

// Card Usage of Calculation
Domo.prototype.getCardUsage = function() {
  return $.get(this.urls.formulaUsage);
}

// Get cards built on dataset
Domo.prototype.getDatasetCards = function() {
  return $.get(this.urls.contentDatasource, { dataSourceId: this.dataSourceId }).then(data => {
    return data.kpiSubscriberList;
  });
}

// Card detail contains card definition
Domo.prototype.getCardDetails = function(cards) {
  const defer = $.Deferred();
  const promises = [];

  $.each(cards, (id, name) => {
    const promise = $.get(this.urls.datasourceSummary, { kpiId: id })
      .then(card => {
        card.cardId = id;
        card.title = name;
        return card;
      });
    promises.push(promise);
  });

  Promise.all(promises).then(values => { defer.resolve(values); });
  return defer.promise();
}

// Return cards that use given beast mode
Domo.prototype.getCardsWithBeastmode = function(cards) {
  const defer = $.Deferred();
  const filteredCards = cards.filter(card => {
    const orders = card.ordering.filter(o => {
      return o.column === this.beastmode;
    });

    const filters = card.whereValueFilter.filter(c => {
      return c.column === this.beastmode;
    });

    return card.indexes.indexOf(this.beastmode) > -1 || card.summaryNumber.valueColumn === this.beastmode || card.aggregations.bigNumber === this.beastmode || orders.length > 0 || filters.length > 0;
  });

  defer.resolve(filteredCards);
  return defer.promise();
}

// helper function to get hostname from a url
function getHostname(url) {
  const a = document.createElement('a');
  a.href = url;
  return a.hostname;
}

// parse url parameters
function getParameterByName(name, url) {
  url = url.toLowerCase();
  name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();
  const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
