// Inject the bridge script
const bridgeScript = document.createElement('script');
bridgeScript.type = 'text/javascript';
bridgeScript.src = chrome.extension.getURL('app/content/inject.js');
document.head.appendChild(bridgeScript);

// Receive messages from background.js
chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
  if (msg.beastmode) { injectControls(msg.beastmode); }
});

// Build custom alert panel with new controls
function injectControls(args) {
  const beastmode = args.formulaId;
  const dataset = args.dataSourceId;

  if ($('.cb-notification-message').length) {
    // Clear existing content
    $('.bm-button').remove();
    $('.cb-notification-message-details').empty();

    // Add new content
    addLinks($('.cb-notification-message-details'), args.usage);
    const $btn = $('<button>', {
      id: 'bm-delete',
      class: 'bm-button float-right',
      text: 'Force Delete',
      click: startDeleteBeastmode,
      dataSourceId: dataset,
      formulaId: beastmode
    });
    const container = $('<div>', { class: 'bm-buttons' });
    container.append($btn);
    $('.cb-notification-message-details').append(container);
  }
}

// Force Delete clicked
function startDeleteBeastmode() {
  createModal();
}

// Confirmation to delete beast mode
function confirmDeleteBeastmode() {
  const message = { delete: $('#bm-delete').attr('formulaId') };
  window.postMessage(JSON.stringify(message), window.location.origin);
  $('.cb-notification-message-container').attr('cb-notification-message--show')
  $('#bm-modal').remove();
}

// Close modal
function cancelDeleteBeastmode() {
  $('#bm-modal').remove();
}

// Add a modal getting confirmation before deleting beast mode
function createModal() {
  $('.bm-modal').remove();

  const html = '' +
    '<div id="bm-modal" class="bm-modal modal-backdrop centered-container trans-fade-1 visible">' +
    ' <div class="modal trans-scale-1">' +
    '   <div class="modal-header">' +
    '     <div class="modal-title"><span>Force Delete Beast Mode</span></div>' +
    '   </div>' +
    '   <div class="modal-body"><span>Are you sure you want to force delete this Beast Mode? This cannot be reversed and will break any cards using the calculation.</span></div>' +
    '   <div class="modal-footer"><div class="modal-buttons"></div></div>' +
    ' </div>' +
    '</div>'

  const modal = $(html);

  modal.find('.modal-buttons').append($('<button>', {
    class: 'button red bm-modal-button',
    text: 'Delete',
    click: confirmDeleteBeastmode
  }));

  modal.find('.modal-buttons').append($('<button>', {
    class: 'button-text bm-modal-button',
    text: 'Cancel',
    click: cancelDeleteBeastmode
  }));

  $('body').append(modal);
}

// Add page navigation for beast mode usage pages
function addLinks(el, usage) {
  usage.sort((a, b) => {
    return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)
  });

  el.text('This calculation cannot be removed as it is being used by the following cards. You can force-delete the beast mode but it will likely break the cards that are using it.');

  // Build List
  const ul = $('<ul>', { class: 'bm-list' });
  usage.forEach(card => {
    const span = $('<span>', {
      class: 'column-label-text',
      text: card.title
    });
    const link = $('<a>', {
      class: 'dm-column-list-column pretty-column bm-link',
      target: '_blank',
      href: 'https://' + location.hostname + '/kpis/details/' + card.cardId + '/edit'
    }).append(span);
    const li = $('<li>', { class: 'bm-list-item' }).append(link);
    ul.append(li);
  });

  el.append(ul);
}

// create button element
function _createButton(id, label, dataSourceId, formulaId, callback) {
  const $btn = $('<button>', {
    id,
    class: 'bm-button float-right',
    text: label,
    click: callback,
    dataSourceId,
    formulaId
  });
  return $btn;
}
