/* Injected from Beast Mode Hunter */

window.addEventListener("message", processMessage, false);

// Process message from extension
function processMessage(event) {
  const msg = JSON.parse(event.data);

  // Modify scope
  if (msg.delete) {
    const ele = $('cb-data-select');
    const $scope = window.angular.element(ele).scope();

    // Remove calculation
    $scope.calculations.models = $scope.calculations.models.filter(model => {
      return model.id !== msg.delete;
    });

    // Remove column
    $scope.columns = $scope.columns.filter(column => {
      return column.id !== msg.delete;
    });

    // Refresh view
    $scope.$apply();
  }
}
